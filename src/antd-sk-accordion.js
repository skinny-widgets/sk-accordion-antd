
import { SkAccordionImpl }  from '../../sk-accordion/src/impl/sk-accordion-impl.js';

export class AntdSkAccordion extends SkAccordionImpl {

    get prefix() {
        return 'antd';
    }

    get suffix() {
        return 'accordion';
    }

    get contentEl() {
        if (! this._contentEl) {
            this._contentEl = this.comp.el.querySelector('.ant-list-items');
        }
        return this._contentEl;
    }

    set contentEl(el) {
        this._contentEl = el;
    }

    get subEls() {
        return [ 'contentEl' ];
    }

    renderTabs() {
        let tabs = this.comp.el.querySelectorAll('sk-tab');
        let num = 1;
        this.tabs = {};
        for (let tab of tabs) {
            let isOpen = tab.hasAttribute('open');
            let title = tab.getAttribute('title') ? tab.getAttribute('title') : '';
            this.contentEl.insertAdjacentHTML('beforeend', `
                <div class="ant-list-item accordion-header${isOpen ? ' accordion-header-active state-active' : ''}" role="tab" 
                    id="header-${num}" tabindex="${num - 1}" data-tab="${num}" ${isOpen ? 'open' : ''}>
                    <span class="ant-list-item-icon">
                        <i aria-label="icon: right" class="anticon anticon-right"><svg viewBox="64 64 896 896" focusable="false" class="" data-icon="right" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M765.7 486.8L314.9 134.7A7.97 7.97 0 0 0 302 141v77.3c0 4.9 2.3 9.6 6.1 12.6l360 281.1-360 281.1c-3.9 3-6.1 7.7-6.1 12.6V883c0 6.7 7.7 10.4 12.9 6.3l450.8-352.1a31.96 31.96 0 0 0 0-50.4z"></path></svg></i>
                        <i aria-label="icon: down" class="anticon anticon-down"><svg viewBox="64 64 896 896" focusable="false" class="" data-icon="down" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M884 256h-75c-5.1 0-9.9 2.5-12.9 6.6L512 654.2 227.9 262.6c-3-4.1-7.8-6.6-12.9-6.6h-75c-6.5 0-10.3 7.4-6.5 12.7l352.6 486.1c12.8 17.6 39 17.6 51.7 0l352.6-486.1c3.9-5.3.1-12.7-6.4-12.7z"></path></svg></i>
                    </span>
                    ${title}
                </div>
                <div ${! isOpen ? 'style="display: none;"' : ''} class="ant-list-pane"
                    id="panel-${num}" role="tabpanel" aria-hidden="${! isOpen ? "true" : "false"}">
                    ${tab.outerHTML}
                </div>        
                 `);

            this.removeEl(tab);
            this.tabs['tabs-' + num] = this.contentEl.querySelector('#header-' + num);
            num++;
        }
    }

    disable() {
        super.enable();
        this.contentEl.querySelectorAll('.accordion-header').forEach((tab) => {
            tab.classList.add('ant-disabled');
        });
    }

    enable() {
        super.disable();
        this.contentEl.querySelectorAll('.accordion-header').forEach((tab) => {
            tab.classList.add('ant-disabled');
        });
    }
}
